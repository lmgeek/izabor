@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('Login') }}</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Vamos!</strong>Hay un Error<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{--<form  role="form" method="POST" action="{{ url('/auth/login') }}">--}}
                            <form method="POST" class="form-horizontal" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('E-Mail Address') }}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember">{{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>

                                    <a class="btn btn-link" href="{{ url('/password/email') }}">{{ __('Forgot Your Password?') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@include('headerFront')



{{--<div class="container-fluid">--}}
    {{--<div class="row">--}}
        {{--<div class="col-xs-12 col-sm-5 col-md-4 login-sidebar" style="margin 0 auto">--}}

            {{--<div class="wide">--}}
                {{--<div class="logo">--}}
                    {{--<img class="img-responsive flip logo hidden-xs animated fadeIn" src="{{ asset('images/logo.png') }}"--}}
                         {{--alt="Logo Icon">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="login-container">--}}

                {{--<p>{{ __('voyager::login.signin_below') }}</p>--}}

                {{--<form action="{{ route('voyager.login') }}" method="POST">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<div class="form-group form-group-default" id="emailGroup">--}}
                        {{--<label>{{ __('voyager::generic.email') }}</label>--}}
                        {{--<div class="controls">--}}
                            {{--<input type="text" name="email" id="email" value=""--}}
                                   {{--placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="form-group form-group-default" id="passwordGroup">--}}
                        {{--<label>{{ __('voyager::generic.password') }}</label>--}}
                        {{--<div class="controls">--}}
                            {{--<input type="password" name="password" placeholder="{{ __('voyager::generic.password') }}"--}}
                                   {{--class="form-control" required>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<button type="button" class="btn btn-block login-button" style="margin-left: 15px"--}}
                            {{--data-toggle="modal" data-target="#myModal">--}}
                        {{--<span class="signin">{{ __('general.registry') }}</span>--}}
                    {{--</button>--}}

                    {{--<button type="submit" class="btn btn-block login-button">--}}
                        {{--<span class="signingin hidden"><span--}}
                                {{--class="voyager-refresh"></span> {{ __('voyager::login.loggingin') }}...</span>--}}
                        {{--<span class="signin">{{ __('voyager::generic.login') }}</span>--}}
                    {{--</button>--}}

                {{--</form>--}}

                {{--<div style="clear:both"></div>--}}

                {{--@if(!$errors->isEmpty())--}}
                    {{--<div class="alert alert-red">--}}
                        {{--<ul class="list-unstyled">--}}
                            {{--@foreach($errors->all() as $err)--}}
                                {{--<li>{{ $err }}</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--@endif--}}

            {{--</div> <!-- .login-container -->--}}

        {{--</div> <!-- .login-sidebar -->--}}
    {{--</div>--}}
{{--</div>--}}



@include('footer');
