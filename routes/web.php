<?php
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
* Home & Web
*/

Route::get('/', function () {
    $posts = App\Post::all();
    $products = App\products::all();
    return view('welcome', compact('posts','products'));
});
Route::get('contacto', function(){
    return view('contacto');
});




/*
* Auth
*/
Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');




/*
* Registro usuario
*/
Route::get('register','Auth\RegisterController@create');
Route::post('register','Auth\RegisterController@create');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});




/*
* post
*/
Route::get('post/{slug}', function($slug){
    $post = App\Post::where('slug', '=', $slug)->firstOrFail();
    return view('post', compact('post'));
});




/*
* Products
*/
Route::get('plato/{id}', function($id){
    $product = App\products::where('id', '=', $id)->firstOrFail();
    return view('products', compact('product'));
});




/*
* Categories
*/
Route::get('category/{id}', function($id){
//    SELECT * FROM products as p INNER JOIN categories as c WHERE c.id = 1
    $category = DB::select( DB::raw('SELECT * FROM products WHERE category_id = '.$id) );
//    $category = App\products::where('category_id', '=', $id)->firstOrFail();
    return view('categories', compact('category'));
});




/*
* Route Cart
*/

Route::resource('cart','Carts');
Route::get('empty',function(){
    Cart::destroy();
});
Route::resource('delete', 'Carts');

Route::post('process', function () {
    return view('cart');
});

Route::get('sales', function () {
    return view('sales');
});





